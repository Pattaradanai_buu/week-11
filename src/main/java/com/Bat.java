package com;

import com.pattaradanai.week11.Animal;

public class Bat extends Animal implements Flyble, Walkable{
    public Bat(String name) {
        super(name, 2);
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep,");
    }

    @Override
    public void eat() {
        System.out.println(this + " eat,");
    }

    @Override
    public String toString() {
        return "Bat ("+this.getName()+")";
    }

    @Override
    public void takeoff() {
        System.out.println(this + " takeoff,");
    }

    @Override
    public void fly() {
        System.out.println(this + " fly,");
        
    }

    @Override
    public void landing() {
        System.out.println(this + " landing,");
        
    }

    @Override
    public void walk() {
        System.out.println(this+ "walk.");
        
    }

    @Override
    public void run() {
        System.out.println(this+ "run.");
        
    }
}
