package com;

public class Plane extends Vehicle implements Flyble {

    public Plane(String name, String eString) {
        super(name, eString);
    }

    @Override
    public void takeoff() {
        System.out.println(this + " takeoff,");
    }

    @Override
    public void fly() {
        System.out.println(this + " fly,");
        
    }

    @Override
    public void landing() {
        System.out.println(this + " landing,");
        
    }
    
    @Override
    public String toString() {
        return "Plane("+getName()+")";
    }
}
