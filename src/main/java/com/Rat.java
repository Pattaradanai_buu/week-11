package com;

import com.pattaradanai.week11.Animal;

public class Rat extends Animal implements Walkable{
    public Rat(String name) {
        super(name, 4);
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep,");
    }

    @Override
    public void eat() {
        System.out.println(this + " eat,");
    }

    @Override
    public String toString() {
        return "rat ("+this.getName()+")";
    }

    @Override
    public void walk() {
        System.out.println(this+ "walk.");
        
    }

    @Override
    public void run() {
        System.out.println(this+ "run.");
        
    }
}
