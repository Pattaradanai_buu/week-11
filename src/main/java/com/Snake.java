package com;

import com.pattaradanai.week11.Animal;

public class Snake extends Animal implements Reptile{

    public Snake(String name) {
        super(name, 0);
    }   

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep,");
    }

    @Override
    public void eat() {
        System.out.println(this + " eat,");
    }
    

    @Override
    public void reptile() {
        System.out.println(this + " Reptile,");
        
    }
}
